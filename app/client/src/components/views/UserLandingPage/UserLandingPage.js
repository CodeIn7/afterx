import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FaCode } from "react-icons/fa";
import MyReservationsForm from '../../containers/MyReservationsForm'



function UserLandingPage() {
   
    return (
        <div >
            <MyReservationsForm/>
        </div>

    )
}

export default UserLandingPage