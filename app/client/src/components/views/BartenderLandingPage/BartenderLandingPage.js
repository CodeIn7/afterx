import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FaCode } from "react-icons/fa";
import styles from './BartenderLanding.module.css'
import ResList from '../../containers/ResList';
import { listReservations } from '../../../_actions/reservation_actions';
import NavBar from '../NavBar/NavBar';



function BartenderLandingPage() {
   
    return (

        <div className={styles.container}>
                    <NavBar></NavBar>
            <ResList />
        </div>

    )
}

export default BartenderLandingPage