const express = require('express');
const router = express.Router();


const order_controller = require('../controllers/order.controller');
//=================================
//             Orders
//=================================

router.get('/', order_controller.order_all);
router.post('/create', order_controller.order_create);
router.get('/:id', order_controller.order_details);
router.put('/:id/update', order_controller.order_update);
router.delete('/:id/delete', order_controller.order_delete);

module.exports = router;
