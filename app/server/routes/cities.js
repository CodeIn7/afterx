const express = require('express');
const router = express.Router();


const city_controller = require('../controllers/city.controller');
//=================================
//             Cities
//=================================

router.get('/', city_controller.city_all);
router.post('/create', city_controller.city_create);
router.get('/:id', city_controller.city_details);
router.put('/:id/update', city_controller.city_update);
router.delete('/:id/delete', city_controller.city_delete);

module.exports = router;
