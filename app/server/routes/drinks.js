const express = require('express');
const router = express.Router();


const drink_controller = require('../controllers/drink.controller');
//=================================
//             Drinks
//=================================

router.get('/', drink_controller.drink_all);
router.post('/create', drink_controller.drink_create);
router.get('/:id', drink_controller.drink_details);
router.put('/:id/update', drink_controller.drink_update);
router.delete('/:id/delete', drink_controller.drink_delete);

module.exports = router;
