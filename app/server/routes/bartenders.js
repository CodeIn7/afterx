const express = require('express');
const router = express.Router();


const bartender_controller = require('../controllers/bartender.controller');
//=================================
//             Bartenders
//=================================

router.get('/', bartender_controller.bartender_all);
router.post('/create', bartender_controller.bartender_create);
router.get('/:id', bartender_controller.bartender_details);
router.put('/:id/update', bartender_controller.bartender_update);
router.delete('/:id/delete', bartender_controller.bartender_delete);

module.exports = router;