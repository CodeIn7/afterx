const express = require('express');
const router = express.Router();


const reservation_controller = require('../controllers/reservation.controller');
//=================================
//             Reservations
//=================================

router.get('/', reservation_controller.reservation_all);
router.post('/create', reservation_controller.reservation_create);
router.get('/:id', reservation_controller.reservation_details);
router.put('/:id/update', reservation_controller.reservation_update);
router.delete('/:id/delete', reservation_controller.reservation_delete);
router.get('/club/:id', reservation_controller.reservation_get_club_reservations);

module.exports = router;
