const express = require('express');
const router = express.Router();


const drinkPackage_controller = require('../controllers/drinkPackage.controller');
//=================================
//             Drink Packages
//=================================

router.get('/', drinkPackage_controller.drinkPackage_all);
router.post('/create', drinkPackage_controller.drinkPackage_create);
router.get('/:id', drinkPackage_controller.drinkPackage_details);
router.put('/:id/update', drinkPackage_controller.drinkPackage_update);
router.delete('/:id/delete', drinkPackage_controller.drinkPackage_delete);

module.exports = router;
