const express = require('express');
const router = express.Router();


const club_controller = require('../controllers/club.controller');
//=================================
//             Cities
//=================================

router.get('/', club_controller.club_all);
router.post('/create', club_controller.club_create);
router.get('/:id', club_controller.club_details);
router.put('/:id/update', club_controller.club_update);
router.delete('/:id/delete', club_controller.club_delete);

module.exports = router;
