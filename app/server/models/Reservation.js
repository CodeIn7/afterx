
const mongoose = require('mongoose');

const reservationSchema = mongoose.Schema({
    userName: {
        type: String
    },
    reservationDate: {
        type: Date
    },
    reservationTime: {
        type: String
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    table: {
        type: mongoose.Types.ObjectId,
        ref: 'Table'
    },
    noPeople: {
        type: Number
    },
    orders: [{
        order: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Order"

        }
    }],
    club:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Club"
    }
})

const Reservation = mongoose.model('Reservation', reservationSchema);

module.exports = { Reservation }
