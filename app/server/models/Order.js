const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    deliveryTime: {
        type: String
    },
    
    footnote: {
        type: String
    },
    
    drinks: [{
        drink: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Drink"

        },
        quantity: {
            type: Number
        }
    }],
    
    drinkPackages: [{
        drinkPackage: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "DrinkPackage"
        },
        quantity: {
            type: Number
        }
    }],
    
    table: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Table"
    }
})

const Order = mongoose.model('Order', orderSchema);

module.exports = { Order }