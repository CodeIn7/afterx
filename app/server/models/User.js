const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const moment = require("moment");
const util = require('util');
var Schema = mongoose.Schema;

function BaseSchema() {
    Schema.apply(this, arguments);

    this.add({
        name: {
            type: String,
            maxlength: 50
        },
        gender: {
            type: String
        },
        email: {
            type: String,
            trim: true,
            unique: 1
        },
        password: {
            type: String,
            minglength: 5
        },
        lastname: {
            type: String,
            maxlength: 50
        },
        birthDate: {
            type: Date
        },
        telephone: {
            type: String,
            maxlength: 15
        },
        role: {
            type: String,
            default: "Consumer"
        },
        image: String,
        token: {
            type: String,
        },
        tokenExp: {
            type: Number
        },
        club: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Club"
        }
    });

    this.pre('save', function (next) {
        var user = this;

        if (user.isModified('password')) {
            // console.log('password changed')
            bcrypt.genSalt(saltRounds, function (err, salt) {
                if (err) return next(err);

                bcrypt.hash(user.password, salt, function (err, hash) {
                    if (err) return next(err);
                    user.password = hash
                    next()
                })
            })
        } else {
            next()
        }
    });

    this.methods.comparePassword = function (plainPassword, cb) {
        bcrypt.compare(plainPassword, this.password, function (err, isMatch) {
            if (err) return cb(err);
            cb(null, isMatch)
        })
    }

    this.methods.generateToken = function (cb) {
        var user = this;
        var token = jwt.sign(user._id.toHexString(), 'secret')
        var oneHour = moment().add(1, 'hour').valueOf();

        user.tokenExp = oneHour;
        user.token = token;
        user.save(function (err, user) {
            if (err) return cb(err)
            cb(null, user);
        })
    }

    this.statics.findByToken = function (token, cb) {
        var user = this;

        jwt.verify(token, 'secret', function (err, decode) {
            user.findOne({ "_id": decode, "token": token }, function (err, user) {
                if (err) return cb(err);
                cb(null, user);
            })
        })
    }
}
util.inherits(BaseSchema, Schema);

var PersonSchema = new BaseSchema();
var BartenderSchema = new BaseSchema({ department: String });

var User = mongoose.model('User', PersonSchema);
var Bartender = User.discriminator('Bartender', BartenderSchema);


module.exports = { User, Bartender }