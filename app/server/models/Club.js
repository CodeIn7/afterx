
const mongoose = require('mongoose');

const clubSchema = mongoose.Schema({
    name: {
        type: String
    },
    capacity: {
        type: Number
    },
    noTables: {
        type: Number
    },
    workingTimeStart: {
        type: String
        //default: dateAndTime.format(date, "hh:mm"),
    },
    workingTimeEnd: {
        type: String
        //default: dateAndTime.format(date, "hh:mm"),
    },
    reservationTelephone: {
        type: String
    },

    address_: {
        city: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "City"
        },
        streetName: {
            type: String
        },
        streetNumber: {
            type: Number
        }
    },
    bartenders: [{
        bartender: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }
    }]
})

const Club = mongoose.model('Club', clubSchema);

module.exports = { Club }
