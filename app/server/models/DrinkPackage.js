const mongoose = require('mongoose');

const drinkPackageSchema = mongoose.Schema({
    name: {
        type: String,
        unique: true
    },
    drinks: [{
        drink: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Drink"
        },
        quantity: {
            type: Number
        }
    }],
    price: {
        type: Number
    }
})

const DrinkPackage = mongoose.model('DrinkPackage', drinkPackageSchema);

module.exports = { DrinkPackage }

