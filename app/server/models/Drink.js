
const mongoose = require('mongoose');

const drinkSchema = mongoose.Schema({
    type: {
        type:String
    },
    name: {
        type:String
    },
    price: {
        type:Number
    },
    quantity: {
        type:Number
    }
})

const Drink = mongoose.model('Drink', drinkSchema);

module.exports = { Drink }
