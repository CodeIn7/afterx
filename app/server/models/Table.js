const mongoose = require('mongoose');

const tableSchema = mongoose.Schema({
    tableNumber: {
        type:Number
    },
    club :{
        type: mongoose.Schema.Types.ObjectId,
        ref:"Club"
    },
    noSeats: {
        type: Number
    },
    typeOfTable: {
        type: String
    },
    noBottles: {
        type:Number
    },
    available: {
        type: Boolean
    },
})
tableSchema.index({ "tableNumber": 1, "club": 1}, { "unique": true });
const Table = mongoose.model('Table', tableSchema);

module.exports = { Table }
