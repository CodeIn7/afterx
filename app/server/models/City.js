const mongoose = require('mongoose');

const citySchema = mongoose.Schema({
    name:{
        type:String,
        unique : true
    },
    clubs:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:"Club"
    }]
})

const City = mongoose.model('City', citySchema);

module.exports = { City }
