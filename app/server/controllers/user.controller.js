const { User } = require("../models/User");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.user_all = function (request, respone) {
    User.find({},(error,result)=> {
        if(error){
            console.error(error);
            return null;
        }
        if(result!=null){
            respone.json(result)
        }
    })
};

exports.user_create = function (request, respone) {
    user = new User(
        {
            postalCode: request.body.postalCode,
            name: request.body.name
        }
    );
    user.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.user_details=function(request,respone){
    User.findById(request.params.id, (err, user)=>{
        if (err) return next(err);
        respone.send(user);
    })
}

exports.user_update = function (request, respone) {
    User.findByIdAndUpdate(request.params.id, {$set: request.body}, function (err, user) {
        if (err) return next(err);
        respone.send('User updated.');
    });
};

exports.user_delete = function (request, respone) {
    User.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};

exports.user_auth = function (request, respone) {
    res.status(200).json({
        _id: req.user._id,
        isAdmin: req.user.role === 0 ? false : true,
        isAuth: true,
        club: req.user.club,
        email: req.user.email,
        name: req.user.name,
        lastname: req.user.lastname,
        role: req.user.role,
        image: req.user.image,
    });
};

exports.user_register = function (request, respone) {
    const user = new User(req.body);

    user.save((err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).json({
            success: true
        });
    });
};

exports.user_login = function (request, respone) {
    User.findOne({ email: req.body.email }, (err, user) => {
        if (!user)
            return res.json({
                loginSuccess: false,
                message: "Auth failed, email not found"
            });

        user.comparePassword(req.body.password, (err, isMatch) => {
            if (!isMatch)
                return res.json({ loginSuccess: false, message: "Wrong password" });

            user.generateToken((err, user) => {
                if (err) return res.status(400).send(err);
                res.cookie("w_authExp", user.tokenExp);
                res
                    .cookie("w_auth", user.token)
                    .status(200)
                    .json({
                        loginSuccess: true, userId: user._id
                    });
            });
        });
    });
};

exports.user_logout = function (request, respone) {
    User.findOneAndUpdate({ _id: req.user._id }, { token: "", tokenExp: "" }, (err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).send({
            success: true
        });
    });
};