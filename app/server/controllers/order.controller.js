const { Order } = require("../models/Order");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.order_all = function (request, respone) {
    Order.find()
        .populate("drinks.drink")
        .populate("table")
        .populate("drinkPackages.drinkPackage")
        .populate("drinks.drink")
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        });
};

exports.order_create = function (request, respone) {
    order = new Order(
        {
            deliveryTime: request.body.deliveryTime,
            footnote: request.body.footnote,
            drinks: request.body.drinks,
            table: request.body.tableId
        }

    );
    order.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.order_details = function (request, respone) {
    Order.findById(request.params.id)
        .populate("drink")
        .populate("table")
        .populate("drinkPackages.drinkPackage")
       // .populate("drinkPackages.drinkPackage.drinks.drink")
        .exec((err, order) => {
            if (err) return next(err);
            respone.send(order);
        });
}

exports.order_update = function (request, respone) {
    Order.findByIdAndUpdate(request.params.id, { $set: request.body }, function (err, order) {
        if (err) return next(err);
        respone.send('Order updated.');
    });
};

exports.order_delete = function (request, respone) {
    Order.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};