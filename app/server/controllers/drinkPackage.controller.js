const { DrinkPackage } = require("../models/DrinkPackage");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.drinkPackage_all = function (request, respone) {
    DrinkPackage.find()
        .populate("drinks.drink")
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        });
};

exports.drinkPackage_create = function (request, respone) {
    drinkPackage = new DrinkPackage(
        {
            name: request.body.name,
            drinks: request.body.drinks,
            price:request.body.price
        }
    );
    drinkPackage.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.drinkPackage_details = function (request, respone) {
    DrinkPackage.findById(request.params.id)
        .populate("drinks.drink")
        .exec((err, drinkPackage) => {
            if (err) return next(err);
            respone.send(drinkPackage);
        });
}

exports.drinkPackage_update = function (request, respone) {
    DrinkPackage.findByIdAndUpdate(request.params.id, { $set: request.body }, function (err, drinkPackage) {
        if (err) return next(err);
        respone.send('DrinkPackage updated.');
    });
};

exports.drinkPackage_delete = function (request, respone) {
    DrinkPackage.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};