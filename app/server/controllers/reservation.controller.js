const { Reservation } = require("../models/Reservation");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.reservation_all = function (request, respone) {
    Reservation.find()
        .populate("user")
        .populate({
            path: "orders.order",
            model: "Order",
            populate: {
                path: "drinks.drink",
                model: "Drink",
                populate: {
                    path: "drink",
                    model: "Drink"
                }
            }
        })
        .populate("table")
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        })
};

exports.reservation_create = function (request, respone) {

    reservation = new Reservation(
        {
            userName: request.body.userName,
            reservationDate: request.body.reservationDate,
            reservationTime: request.body.reservationTime,
            user: request.body.userId,
            table: request.body.tableId,
            noPeople: request.body.noPeople,
            orders: request.body.orders,
            club: request.body.clubId
        }

    );
    reservation.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.reservation_details = function (request, respone) {
    Reservation.findById(request.params.id)
        .populate("user")
        .populate({
            path: "orders.order",
            model: "Order",
            populate: {
                path: "drinks.drink",
                model: "Drink",
                populate: {
                    path: "drink",
                    model: "Drink"
                }
            }
        })
        .populate("table")
        .exec((err, reservation) => {
            if (err) return next(err);
            respone.send(reservation);
        });
}

exports.reservation_update = function (request, respone) {
    Reservation.findByIdAndUpdate(request.params.id, { $set: request.body }, function (err, reservation) {
        if (err) return next(err);
        respone.send('Reservation updated.');
    });
};

exports.reservation_delete = function (request, respone) {
    var res = {}
    Reservation.findById(request.params.id, function (err, reservation) {
        res = reservation
    });
    Reservation.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send(res);
    })
};
exports.reservation_get_club_reservations = function (request, respone) {

    Reservation.find()
        .where('club')
        .equals(request.params.id)
        .populate("user")
        .populate({
            path: "orders.order",
            model: "Order",
            populate: {
                path: "drinks.drink",
                model: "Drink",
                populate: {
                    path: "drink",
                    model: "Drink"
                }
            }
        })
        .populate({
            path: "orders.order",
            model: "Order",
            populate: {
                path: "drinkPackages.drinkPackage",
                model: "DrinkPackage",
                populate: {
                    path: "drinks.drink",
                    model: "Drink",
                    populate: {
                        path: "drink",
                        model: "Drink"
                    }
                }
            }
        })
        .populate("table")
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        })

};