const { User, Bartender } = require("../models/User");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.bartender_all = function (request, respone) {
    Bartender.find({},(error,result)=> {
        if(error){
            console.error(error);
            return null;
        }
        if(result!=null){
            respone.json(result)
        }
    })
};

exports.bartender_create = function (request, respone) {
    bartender = new Bartender(
        {
            postalCode: request.body.postalCode,
            name: request.body.name
        }
    );
    bartender.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.bartender_details=function(request,respone){
    Bartender.findById(request.params.id, (err, bartender)=>{
        if (err) return next(err);
        respone.send(bartender);
    })
}

exports.bartender_update = function (request, respone) {
    Bartender.findByIdAndUpdate(request.params.id, {$set: request.body}, function (err, bartender) {
        if (err) return next(err);
        respone.send('Bartender updated.');
    });
};

exports.bartender_delete = function (request, respone) {
    Bartender.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};