const { City } = require("../models/City");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.city_all = function (request, respone) {
    City.find({},(error,result)=> {
        if(error){
            console.error(error);
            return null;
        }
        if(result!=null){
            respone.json(result)
        }
    })
};

exports.city_create = function (request, respone) {
    city = new City(
        {
            postalCode: request.body.postalCode,
            name: request.body.name
        }
    );
    city.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.city_details=function(request,respone){
    City.findById(request.params.id, (err, city)=>{
        if (err) return next(err);
        respone.send(city);
    })
}

exports.city_update = function (request, respone) {
    City.findByIdAndUpdate(request.params.id, {$set: request.body}, function (err, city) {
        if (err) return next(err);
        respone.send('City updated.');
    });
};

exports.city_delete = function (request, respone) {
    City.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};