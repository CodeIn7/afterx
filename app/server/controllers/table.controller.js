const { Table } = require("../models/Table");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.table_all = function (request, respone) {
    Table.find()
        .populate("club")
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        });
};

exports.table_create = function (request, respone) {
    table = new Table(
        {
            tableNumber: request.body.tableNumber,
            club: request.body.club,
            noSeats: request.body.noSeats,
            typeOfTable: request.body.typeOfTable,
            noBottles: request.body.noBottles,
            available: request.body.available
        }
    );
    table.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.table_details = function (request, respone) {
    Table.findById(request.params.id)
        .populate("club")
        .exec((err, table) => {
            if (err) return next(err);
            respone.send(table);
        });
}

exports.table_update = function (request, respone) {
    Table.findByIdAndUpdate(request.params.id, { $set: request.body }, function (err, table) {
        if (err) return next(err);
        respone.send('Table updated.');
    });
};

exports.table_delete = function (request, respone) {
    Table.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};

exports.table_club = function (request, respone) {
    Table.find()
        .where('available')
        .equals(true)
        .where("club")
        .equals(request.params.id)
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        });
};