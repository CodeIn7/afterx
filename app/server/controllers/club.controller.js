const { Club } = require("../models/Club");
const { City } = require("../models/City")
const mongoose = require('mongoose');
//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.club_all = function (request, respone) {
    Club.find()
        .populate('address_.city')
        .populate('bartenders.bartender')
        .exec((error, result) => {
            if (error) {
                console.error(error);
                return null;
            }
            if (result != null) {
                respone.json(result)
            }
        });
    //Club.find({}, )
};

exports.club_create = function (request, respone) {

    club = new Club(
        {
            _id: new mongoose.Types.ObjectId(),
            name: request.body.name,
            capacity: request.body.capacity,
            noTables: request.body.noTables,
            workingTimeStart: request.body.workingTimeStart,
            workingTimeEnd: request.body.workingTimeEnd,
            reservationTelephone: request.body.reservationTelephone,
            address_: {
                city: request.body.cityId,
                streetName: request.body.streetName,
                streetNumber: request.body.streetNumber
            },
            bartenders: request.body.bartenders
        }
    );

    City.findByIdAndUpdate(request.body.cityId, { $push: { "clubs": club._id } }, function (err, city) {
        if (err) return next(err);
    });

    club.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });

};

exports.club_details = function (request, respone) {

    Club.findById(request.params.id)
        .populate('address_.city')
        .populate('bartenders.bartender')
        .exec((err, club) => {
            if (err) return next(err);
            respone.send(club);
        });

}

exports.club_update = function (request, respone) {
    console.log(request.body)
    Club.findByIdAndUpdate(request.params.id, { $set: request.body }, function (err, club) {
        if (err) return next(err);
        respone.send('Club updated.');
    });
};

exports.club_delete = function (request, respone) {
    Club.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};