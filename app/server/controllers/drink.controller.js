const { Drink } = require("../models/Drink");

//Simple version, without validation or sanitation
exports.test = function (request, respone) {
    respone.send('Greetings from the Test controller!');
};

exports.drink_all = function (request, respone) {
    Drink.find({},(error,result)=> {
        if(error){
            console.error(error);
            return null;
        }
        if(result!=null){
            respone.json(result)
        }
    })
};

exports.drink_create = function (request, respone) {
    drink = new Drink(
        {
            type: request.body.type,
            name: request.body.name,
            price: request.body.price,
            quantity: request.body.quantity
        }
    );
    drink.save((err, doc) => {
        if (err) return respone.json({ success: false, err });
        return respone.status(200).json({
            success: true
        });
    });
};

exports.drink_details=function(request,respone){
    Drink.findById(request.params.id, (err, drink)=>{
        if (err) return next(err);
        respone.send(drink);
    })
}

exports.drink_update = function (request, respone) {
    Drink.findByIdAndUpdate(request.params.id, {$set: request.body}, function (err, drink) {
        if (err) return next(err);
        respone.send('Drink updated.');
    });
};

exports.drink_delete = function (request, respone) {
    Drink.findByIdAndRemove(request.params.id, function (err) {
        if (err) return next(err);
        respone.send('Deleted successfully!');
    })
};